# Wearable

According to Statista, the worldwide number of connected wearable devices increased from 325 million in 2016 to 722 million in 2019, and it is expected to reach more than 1 billion in 2022! So, developers and businesses should be aware of this trend.          

## What are wearable devices?
Wearables are electronic devices with microcontrollers and smart sensors that we can wear or carry in our body and that we can connect to our mobile (using Bluetooth, Wi-Fi or cellular network) to sync gathered data that we can later analyze. Although the range of wearables has increased in recent years, they are mainly divided in three categories: smartwatches, wristbands and hearables.          

They became very popular with step counters and activity tracking to help us achieve a healthier and more active life. They also help us track our sleep and notify us to move regularly during our daily routine. With the irruption of COVID-19, the demand for this type of products has spiked with consumers asking for more in wearable devices and apps that entertain spot practice. However, today the possibilities are endless and smartwatches have become almost an extension of our mobile phones enabling us to respond to messages without taking our phone out of our pocket, make payments and even control home devices.             

Apple is the leading wearable vendor thanks to the Apple Watch followed by Xiaomi, Huawei and Samsung.          

## Industry
Both Apple and Google are betting on the wearable device industry. In 2019 Google announced the acquisition of Fitbit in USD 2.100 million, one of the most important manufacturers of sport watches and activity bracelets which was finally materialized in 2021. This was a big investment to improve the smartwatch platform and health apps. Also, in 2021, Google announced a unified wearable platform with Tizen (Samsung’s software platform) with the aim of improving performance (they claim apps start up to 30% faster) and power efficiency. Both Google Assistant and Google Maps were improved, Google Pay added new support for more countries (hopefully Uruguay will come soon!) and YouTube Music was also recently added on Wear. Now you can download your music directly to your watch and leave your phone at home when going for a walk.              
On the other hand, in mid 2021, Apple launched the beta version of WatchOS 8 which included new features such as new watch faces, increased wallet access, a new Mindfulness app (previous Breathe app) and a redesign interface for Apple Watch users.                 

Consumers can now customize their experience with personalized watch faces, designer straps, and even designers are incorporating Wear OS to meet their consumers demand. More than 85% of connected wearable devices are concentrated in North America, Asia and Western Europe with North America taking the lead.                



## What about developers?
Google has invested in making it easier for developers to create new experiences for wearables. They have included new Wear APIs to the Jetpack Library, upgraded tools in AndroidStudio and improved the distribution in the Google PlayStore. For example, the Tiles API enables any developer to create a custom tiles for any app and customize the home screen carousel, the Ongoing Activity API makes it easy to switch back and forth between apps and manage multiple tasks, and the Health Services (alpha release), which is a health and fitness platform for Android, allows developers to get data from only one source instead of having to configure and connect to the multiple sensors to gather the raw information.                 

On the Apple side, they have also focused on providing tools to create applications that improve the user experience. With Always-On Display the UI adapts it’s brightness and displayed information to the user interaction, they also incorporated region based user notifications, and HealthKit Data acts as a central repository for health and fitness data that your app can access and watchOS 8 delivers that info in the background which is very important for standalone watch apps.                

## Conclusion
So, are smartwatches the new mobile phones? Still there is a long way to go before we see that happening but new apps can be developed to bridge the gap and with consumers definitively incorporating them into their daily routines it gives an extra incentive to develop new features for them.             
